#!/bin/bash

##############################################################################
#                           Build a .md page                                 #
#     including a hyperlink to each of the files in a given directory,       #
#    e.g. a subdirectory of "docs/", typically in a MkDocs architecture,     #
#               where a CI pipeline could invoke this script,                #
#          before writing html files with `mkdocs build` command...          #
#                                                                            #
#                      PROVIDED WITHOUT ANY GUARANTY                         #
#             under WTFPL (http://www.wtfpl.net/txt/copying/)                #
#            by Jean-Marc Gervais (gervaisprof at free dot fr)               #
##############################################################################

##### Params and page content customization #####
# /!\  Remember: no spaces around "=" in shell for assignations...

# Markdown documents directory
DOCS=docs
#Subdirectory in $DOCS that contain distributed files to list (e.g. .pdf)
DIR=00_Docs
# Markdown page to create
PAGE=$DOCS/$DIR/index.md
# Name of header (h2 level) for "Uncategorized files"
OTHER="Autres"
# Optional footer content (in MarkDown)
FOOTER="*Merci de m'indiquer les coquilles quand vous en découvrez...*\n\
\nLes collègues qui sont intéressés par les sources (\`.odt\`,\
 \`.tex\`, etc.) peuvent me contacter via leur adresse email\
 académique (pour déterminer la mienne : je suis Jean-Marc Gervais, dans\
 l'académie de Grenoble)."


##### Tools #####

# Add first param (string) into $PAGE (followed by "\n")
function add2md() {
    echo -e "$1" >> $PAGE
}

# Add a link to a document (opened in a new tab) into $PAGE
# . First param is the string describing the hyperlink
# . Second one is the URL
# . Third one is the file name
function addlink(){
    # Notebooks => download + direct use into Basthon
    # (only available for online site, not locally)
    if [ "${2##*.}" = 'ipynb' ]
    then
        short_name=$(basename "$2" ".${2##*.}")   # basename $NAME $SUFFIX
        if [ "${short_name##*.}" = 'sql' ]
        then
            add2md "- [\`$1\`]($2){:download=$3} [(↪Ⓑ)](https://notebook.basthon.fr/?kernel=sql&from=$SITE/$DIR/$2){:target=\"_blank\"}"
        else
            add2md "- [\`$1\`]($2){:download=$3} [(↪Ⓑ)](https://notebook.basthon.fr/?from=$SITE/$DIR/$2){:target=\"_blank\"}"
        fi
    # Other files => new page for PDF, else download
    else
        if [ "${2##*.}" = 'pdf' ]
        then
            add2md "- [\`$1\`]($2){:target=\"_blank\"}"
        else
            add2md "- [\`$1\`]($2){:download=$3}"
        fi
    fi
}

# Transform real file name (param) in its version to print
# . First param for directory (ended by "/")
# . Second one for file name
function adaptname(){
    # Indicate size only when it is > 1MB
    size=$(du $1$2 | cut -f1)
    sizeh=$(du -h $1$2 | cut -f1)
    if [ $size -gt 1024 ]; then
        size=" [${sizeh}o]"
    else 
        size=""
    fi

    # (Specific to my personal naming: short file names)
    # Erase "_2324", "_1NSI" and "_TNSI" substrings
    # and replace underscores by spaces
    filename=${2//_2324/}   # $2 gives the file name
    filename=${filename//_2de/}
    filename=${filename//_/ }
    
    echo "$filename$size"
}


###### Main #####
# Site's URL ($CI_PAGES_URL as first parameter of the script during CI)
SITE=$1

# 1. Reset $PAGE & write its header
echo "---" > $PAGE
echo 'title: "⛭ Documents distribués"' >> $PAGE
echo "author: Jean-Marc Gervais" >> $PAGE
echo "---" >> $PAGE
echo "" >> $PAGE

# Note that nothing specific is done if $DIR is an empty directory


# 2. List files in $DOCS/$DIR and add hyperlinks to them into $PAGE
# grouped by subdirectory, at first level only, 
# only if the subdir is not empty and doesn't contain a file named ".unlisted"
# 
# Note here '-' is used for list bullets because "*" would be interpreted
# by shell

# 2.a.  Files in a $DOCS/$DIR subdirectory (only available for first level)
file_exists_out_of_dir=false    # For 2.b.

for subdir in `ls $DOCS/$DIR/`
do  # Directory, neither empty nor excluded
    if [ -d "$DOCS/$DIR/$subdir" \
         -a   ! -z "$(ls $DOCS/$DIR/$subdir)" \
         -a   ! -e "$DOCS/$DIR/$subdir/.unlisted" ] 
    then
        # Header (h2 level): subdirectory name
        add2md "## $subdir"

        # Files listing
        for file in `ls $DOCS/$DIR/$subdir/`
        do
            description=$(adaptname "$DOCS/$DIR/$subdir/" $file)
            addlink "$description" "$subdir/$file" "$file"
        done
    elif [ -f "$DOCS/$DIR/$subdir" ]
    then
        file_exists_out_of_dir=true
    fi
done

# 2.b.  Files directly stored in $DOCS/$DIR
# Header (h2 level), only if there is at least one file outside a subdirectory
if $file_exists_out_of_dir
then
    add2md "## $OTHER"
fi

# Files listing
for file in `ls $DOCS/$DIR`
do  # Only for standard file, not directories
    if [ -f $DOCS/$DIR/$file  -a  $file != "index.md" ]
    then
        description=$(adaptname "$DOCS/$DIR/" $file)
        addlink "$description" "$file" "$file"
    fi
done

# 3. Footpage
add2md "\n---\n"
if [ ! -z "$FOOTER" ]
then
    add2md "$FOOTER"
fi

