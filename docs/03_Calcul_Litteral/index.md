---
title : Calcul Littéral — CL1
author: Jean-Marc Gervais
---

??? note "[Documents en lien avec cette partie](../00_Docs/#03_calcul_litteral){:target="_blank"} (en PDF)"
    Ils sont récupérables [ici](../00_Docs/#03_calcul_litteral){:target="_blank"} :
    
    * Le **cours**
    
    * Des TD d'entraînement, avec corrigé : développer, supprimer des parenthèses 
    et réduire ; résoudre des équations (dont les produits nuls), des inéquations ; 
    exprimer une variable en fonction des autres en partant d'une formule donnée...
    
    * Quelques problèmes qui se ramènent à une résolution d'équation.

Outre le calcul littéral à proprement parler (sans traiter pour l'instant des
factorisations), on a rappelé la méthodologie pour résoudre des 
problèmes qui se ramènent à une résolution d'(in)équation.

!!! tip "Conseil..."
    Là encore, comme pour le calcul numérique non littéral, seule **la régularité**
    permet d'acquérir suffisamment d'automatismes, d'aisance, pour pouvoir se
    concentrer sur les nouvelles notions quand elles mettront en œuvre ces 
    calculs.
    
    Évidemment, si l'on repère des lacunes au niveau du 
    [calcul purement numérique](../01_Calcul_Numerique) 
    (avec des fractions, puissances ou autres), il est important de
    les combler rapidement, pour ne pas cumuler les difficultés et pour que les
    progrès en calcul littéral puissent être correctement récompensés lors des 
    évaluations.

## Développer, réduire...

L'idée est de transformer une expression (formule) en une autre identique
(égale pour toutes les valeurs possibles de la variable), **qui s'écrit sans parenthèses**. 
C'est souvent utile par exemple pour calculer des images de réels par une fonction.

!!! note "À maîtriser"

    === "Développer"
        * Utilisation de la distributivité de la multiplication sur l'addition et la soustraction (*donc pour les cas avec un $\times$ devant une parenthèse*) :        
          Cas simples basés directement sur $\quad k(a+b) = ka + kb\quad$ ou $\quad k(a-b) = ka - kb\quad$    
          comme par exemple pour développer $\quad7(9a+4)\quad$ ou $\quad2x(11x-2)$.

        * « Double distributivité », du type $\quad(a + b)(c + d) = ac + ad + bc + bd\quad$ avec des réels $a$, $b$, $c$ et $d$ de signes quelconques.    
          Par exemple pour développer $\quad(7t - 1)(6t + 4)\quad$ (avec $~a=7t$, $~b=-1$, etc.)
        
    === "Identités remarquables"
        Cas particuliers d'expressions qu'on gagne à développer à l'aide d'une identité remarquable :
        ??? question "Écris-les *avant* de vérifier, donne des exemples" 
            - $(a+b)^2 \quad=\quad a^2 + 2ab + b^2$
            - $(a-b)^2 \quad=\quad a^2 - 2ab + b^2$
            - $(a+b)(a-b) \quad=\quad a^2 - b^2$
        
            Exemples :     
            $(3x-5)^2 = (3x)^2 - 2\times3x\times5 + 5^2 = 9x^2 - 30x + 25$    
            $(7x-11)(7x+11) = (7x)^2 - 11^2 = 49x^2 - 121$
        
        *Apprends à bien les repérer dans les calculs...*

    === "Supprimer les parenthèses"
        Utilisation des règles de suppression de parenthèses précédées d'un signe $+$ ou d'un signe $-$ :     
        $\quad\cdots + (a + b - c) = \cdots + a + b - c\quad$ et     
        $\quad\cdots - (a + b - c) = \cdots - a - b + c$
        
        On peut se ramener aux règles de distributivité, en remarquant que     
        $\quad+(\cdots) = + 1(\cdots)\quad$ et $\quad-(\cdots) = -1(\cdots)$

        ??? question "$7x - (5x - 3 + 4y) + (-6 - 8x + y) = \cdots$"
            $\phantom{=~}7x - (5x - 3 + 4y) + (-6 - 8x + y)$    
            $=~7x - 5x + 3 - 4y - 6 - 8x + y$    
            $=~\cdots = -6x -3y - 3$
            
    === "Réduire"
        Penser que toute expression développée est ensuite réduite. Bien distinguer :

        * Avec $+$ et $-$, pour s'en souvenir, c'est un peu comme « 2 pommes + 3 pommes = 5 pommes » (tandis que « 2 pommes + 3 clous » n'est pas simplifiable) : on regroupe entre eux les termes qui ont la même partie littérales. *Et l'on peut démontrer cela grâce à la distributivité...*
        * Avec $\times$, on utilise le fait que l'on peut changer l'ordre et regrouper les termes comme on le souhaite quand il n'y a que des multiplications :     
          $2a \times 5a^2 = 2 \times a \times 5 \times a \times a = 2 \times 5 \times a \times a \times a = 10a^3$
        
        <center>

        | Avec $+$ et $-$ |    | Avec $\times$ |
        |:---:           | :--: | :---: |
        | ${2a + 3a = 5a,}\quad{2a - 3a = -a}$| alors que | $2a \times 3a = 6a^2$ |
        | $2a + 3a^2~$ ou $~2a - 3a^2~$ ne se réduisent pas| mais | $2a \times 3a^2 = 6a^3$ |
        | $2x + 3~$ ou $~2x - 3~$ ne se réduisent pas| mais | $2x \times 3 = 6x$ |
        | $2a + 3b~$ ou $~2a - 3b^2~$ ne se réduisent pas| mais | $2a \times 3b = 6ab$ |

        </center>

!!! success "Pour s'entraîner"

    * [Entraînement en ligne sur MathAléa](https://coopmaths.fr/alea/?uuid=f6853&id=3L11-2&n=10&d=10&s=false&s2=false&s3=9&alea=wFyo&cd=1&uuid=603a8&id=3L10&n=10&d=10&s=1-3-4-5-6-7-8-9-10&alea=Hcoo&cd=1&uuid=77a62&id=3L11&n=10&d=10&s=3&s2=1&s3=8&s4=false&alea=78QK&cd=1&uuid=c8403&id=3L11-10&n=5&d=10&s=3&alea=gflQ&cd=1&uuid=4197c&id=3L11-1&n=10&d=10&s=3&s2=true&alea=7oZK&cd=1&uuid=82313&id=3L11-3&n=10&d=10&alea=e3Nx&cd=1&v=eleve&z=0.9&title=D%C3%A9velopper+-+R%C3%A9duire+-+Supprimer+des+Parenth%C3%A8ses&es=021100){:target="_blank"}
    * Passer le [permis](https://www.mathix.org/permis_litteral){:target="_blank"} sur Mathix, ça te dit ?

## Équations

!!! note "L'essentiel"

    === "Règles de base"
        Pour transformer une équation en une autre équivalente (qui a les mêmes solutions) :
        ??? question "Écris-les *avant* de vérifier..." 
            - on peut **additionner ou soustraire** le même nombre aux deux membres de l'équation.
            - on peut les **multiplier ou** les **diviser** par un même nombre **non nul**.
        
        La bonne stratégie consiste en général à placer à l'aide de ces règles les termes d'un type (ex. « en $x$ ») dans un membre et les autres (ex. les constantes) dans l'autre membre de l'équation.
        
    === "$\mathcal S = \cdots$"
        En seconde, on prendra l'habitude d'indiquer un *ensemble des solutions* généralement noté $\mathcal S$ à la fin de toute résolution.

    === "Produits nuls"
        Équations « produit nul » :     
        > *Quand on arrive à une équation de degré 2, comme $\quad ax^2+bx+c=0,\quad$ on ne sait en général pas la résoudre. Par contre, cela peut nous indiquer qu'on aurait dû la ramener à un produit nul plutôt que de développer...* 
        
        Règle utilisée :     
        $A\times B = 0\quad$ équivaut à $\quad A = 0~\text{  ou  }~B = 0$...
  
        Sais-tu résoudre l'équation $\quad(2x+3)(9x-7)=0$ ?$\quad$Et $\quad (x-2)-(6x+5)=0$  ?
        
        ??? question "Ensemble $\mathcal{S}$ des solutions ?"
            Pour la première équation, on arrive à 
            $\mathcal{S} = \left\{ -\frac{3}{2};\frac{7}{9}\right\}$    
            Pour la seconde, *attention, on reste attentif*, il ne s'agit pas 
            d'un **produit** nul. Après suppression des parenthèses, on arrive 
            à $\mathcal{S} = \left\{-\frac{7}{5}\right\}$.
            
## Inéquations

On n'a abordé pour l'instant que les plus simples (de degré 1). Leur résolution est très proche de celle des équations, mais les règles sont un peu différentes :

??? question "Écris-les *avant* de vérifier pour réviser efficacement"
    Pour transformer une inéquation en une autre équivalente (qui a les mêmes solutions),
    
    * on peut **additionner ou soustraire** le même nombre aux deux membres (en conservant le signe d'inégalité).
    * on peut les **multiplier ou diviser** par un même nombre **strictement positif** (en conservant le signe d'inégalité).
    * on peut les **multiplier ou diviser** par un même nombre **strictement négatif**, à condition de **renverser le signe d'inégalité*.    
      (Le $<$ devient $>$, le $\leqslant$ devient $\geqslant$, etc.)
    
Vérifie que tu sais résoudre cette petite inéquation : $\quad3x+8<9x-3$

??? question "Ensemble $\mathcal{S}$ des solutions ?"
    On arrive à $\quad x~>~\frac{11}{6},\quad$ d'où : $\quad\mathcal{S}~=~\left]\frac{11}{6};+\infty\right[$
    

