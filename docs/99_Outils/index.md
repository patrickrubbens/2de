---
title : ⴲ Boîte à Outils
author: Jean-Marc Gervais
---

## Entraînement aux calculs

(N'hésitez pas à reprendre des notions répertoriée dans les parties 
« collège », tant qu'elles ne sont pas acquises)

* [MathEnPoche](https://mathenpoche.sesamath.net/){:target="_blank"} reste 
une ressource de référence, même si le site peut paraître un peu démodé.

* [MathAléa](https://coopmaths.fr/alea/){:target="_blank"} permet de s'entraîner en ligne sur des 
exercices variés corrigés, sur des thèmes de son choix (j'ai inclus dans 
différents chapitres sur ce site des liens vers des activités déjà configurées).

## Émulateurs de calculatrice

Ils peuvent servir en cas d'oubli, en dehors du lycée, pour découvrir d'autres
modèles ou si l'on ne dispose uniquement d'un modèle de collégien et que ça 
ne suffit pas pour certaines activités.

*Sans aucun conflit d'intérêt, nous nous permettons au lycée d'afficher notre
préférence pour la Numworks...*

* **Numworks** fournit un [simulateur](https://www.numworks.com/fr/simulateur/){:target="_blank"}
de sa calculatrice de type lycée. Il est possible de le télécharger pour 
l'utiliser hors connexion.    
  Il existe également une application à installer sur téléphone ou tablette
(cherchez `Numworks` dans les *stores* de votre marque d'appareil)    
  *Dans les deux cas, on ne peut pas enregistrer son travail, ce qui est 
possible si l'on se crée un compte sur le site de la marque...*

* Certains ont créé des systèmes d'exploitation alternatifs pour ces 
calculatrices Numworks, vous trouverez ainsi des émulateurs comme celui 
d'[**Omega**](https://getomega.dev/simulator){:target="_blank"}, avec 
quelques fonctionnalités supplémentaires par rapport au modèle de base.

* **Texas Instruments** fournit également un [émulateur en ligne](https://maclasseti.fr/calculatrice){:target="_blank"}
pour sa TI83 Premium CE.

* je n'ai pas trouvé d'outil en ligne, mais il existe des simulateurs à 
installer pour **Casio** et **HP**, répertoriés 
[ici](https://www.maths-et-tiques.fr/index.php/outils-pour-le-prof/emulateurs-calculatrices){:target="_blank"}

## Programmation Python

* Ça n'est pas forcément le plus pratique au niveau de la saisie, mais les 
**calculatrices de type lycée** dont la Numworks permettent de programmer en 
Python (idem pour les applis ou émulateurs, mais on ne peut généralement pas
enregistrer son travail, ce qui limite l'intérêt).

* Pour disposer d'une **console Python et** d'une **zone « programme »** sans 
rien  installer, on peut utiliser 
[**Basthon**](https://console.basthon.fr/){:target="_blank"}

* On peut aussi créer ou reprendre des **_notebooks_** déjà enregistrés, toujours
sur le site de [Basthon](https://notebook.basthon.fr/){:target="_blank"}    
  Si besoin, *Fichier / Nouveau Notebook / Python 3* permet de retrouver une 
page vierge si le navigateur a conservé la trace d'un précédent travail. Et
pour reprendre un *notebook* enregistré, il ne faut pas double-cliquer dessus
mais partir de ce site et passer par *Fichier / Ouvrir* (ou la deuxième icône).

* Les curieuses et curieux pourront approfondir leur découverte du langage
Python sur [FutureCoder](https://futurecoder.forge.aeif.fr/){:target="_blank"}

* Ou alors, sous une forme plus ludique, sur [Py-rates](https://py-rates.fr/){:target="_blank"}
(assez court, bien conçu).

* L'[AEIF](https://e-nsi.forge.aeif.fr/init_python/1-print/10-hello/){:target="_blank"}
(Association des Enseignants et Enseignantes d'Informatique de France), dont je 
suis adhérent, propose également un cours pour faire ses premiers pas, bien 
structuré.


## Géométrie dynamique

**GeoGebra** est l'outil incontournable. On le retrouve sous forme d'appli. 
pour téléphone ou tablette, ou sur le web en version
[géométrie plane](https://www.geogebra.org/geometry){:target="_blank"},
[géométrie dans l'espace](https://www.geogebra.org/3d){:target="_blank"},
[« présentation classique »](https://www.geogebra.org/classic){:target="_blank"}
similaire aux versions installées au lycée,
[ou autres](https://www.geogebra.org/){:target="_blank"}...

## Calcul formel

*Il s'agit de travailler en valeur exacte et sur des expressions littérales, 
ces outils peuvent être utiles pour vérifier son travail, ou pour résoudre
des problèmes qu'on est pas (encore) capable de traiter à son niveau...*

* [GeoGebra](https://www.geogebra.org/cas){:target="_blank"} permet entre 
autres de faire du calcul formel (existe aussi en appli pour téléphone ou
tablette).

* [XCas](https://www.xcasenligne.fr/){:target="_blank"} est plus puissant, 
il est utilisable en ligne (choisir la seconde icône « baguette magique » à 
gauche pour choisir le calcul à effectuer, sans avoir besoin de connaître 
les commandes à saisir).
