---
title : Fonctions (bases, variations, parité)
author: Jean-Marc Gervais
---

??? note "[Documents en lien avec cette partie](../00_Docs/#04_fonctions){:target="_blank"} (en PDF)"
    Ils sont récupérables [ici](../00_Docs/#04_fonctions){:target="_blank"} :
    
    * Le **cours**, qui comprend plusieurs exemples à traiter, avec le corrigé
    de certains dans des documents indépendants.
    
    * Un TD d'application, pour étudier l'aire d'une figure géométrique suivant
    la position d'un point donné.
    
    * Des corrigés des exercices du manuel.

Le concept de fonction est l'un des plus importants en mathématiques et dans 
les autres domaines liés (sciences physiques, économie, etc.) Si l'on étudie
souvent en maths des fonctions déconnectée d'un contexte particulier, c'est
pour se simplifier la vie et s'entraîner sans avoir à inventer une histoire à
chaque fois, mais le but est au final de les utiliser dans des cas concrets, 
notamment dans les problèmes d'optimisation.

## Fondamentaux

**Prérequis** : il est important de 

* bien connaître le vocabulaire spécifique : **image**, **antécédent**, 
**domaine** (ou ensemble) **de définition**, etc.

* ne plus hésiter sur les termes liés à la représentation graphique, notamment
« **abscisse** » et « **ordonnée** » et savoir où **comment les lire** sur un 
graphique ou **comment placer un point** dans un repère quand on les connaît.

* avoir suffisamment acquis d'automatismes sur les calculs de base (reprendre
l'entraînement aussi longtemps que nécessaire...)

---
**Questions types** : pour une fonction donnée $f$, on peut ramener un grand 
nombre de questions aux deux suivantes :

1. « On considère un nombre de départ (disons 13): quelle est son image par la
fonction ? »    
   <center>$f: 13 ~\longmapsto~ ??\quad$ autrement dit 
   $\quad f(13)=~??$</center>
2. « Pour un nombre donné (disons 123), quels sont ses antécédents par la
fonction ? »    
   <center>$f: ~?? ~\longmapsto~ 123\quad$ autrement dit 
   $\quad f(??)=123$</center>
   
Écris ou représente sur un schéma ce qu'il faut faire pour répondre à chacune 
de ces deux questions, 

* dans le cas où l'on dispose de la courbe de la fonction, ou 
* dans le cas où l'on connaît une expression (formule) de la fonction

??? question "*Se jeter sur la solution sans rien écrire = perdre son temps...*"
    1. Pour la recherche de l'**image**, $\quad f: 13 ~\longmapsto~ ??$ :
        - **Avec la formule**, on remplace la variable par 13 et on effectue un
        **calcul direct**.
        - **Avec la courbe**, on place **13 sur l'axe des abscisses** 
        (« horizontal ») et **on lit sur l'autre axe l'ordonnée** du point de 
        la courbe de $f$ qui a cette abscisse.
        <center>![lecture d'image](img/image.png)</center>
        
    2. Pour la recherche des **antécédents**, $\quad f: ~?? ~\longmapsto~ 123$ :
    
        *Il peut y avoir zéro, un ou plusieurs antécédents, selon les cas...*
        
        - **Avec la formule**, on résout l'**équation** $\quad f(x) = 123,\quad$ 
        dont les solutions sont les antécédents recherchés.
        - **Avec la courbe**, on place **123 sur l'axe des ordonnées** 
        (« vertical ») et on lit sur l'autre axe *les* abscisses de *tous les 
        points* de la courbe de $f$ qui ont cette ordonnée.
        <center>![lecture des antécédents](img/antecedents.png)</center>
        
## Variations, extremums

!!! warning "À compléter..."

## Parité

!!! warning "À compléter..."


