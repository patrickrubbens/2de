---
title : Repérage - Géométrie plane
author: Jean-Marc Gervais
---

??? note "[Documents du chapitre](../00_Docs/#06_reperage_geometrie){:target="_blank"} (en PDF)"
    Ils sont récupérables [ici](../00_Docs/#06_reperage_geometrie){:target="_blank"} :
    
    * COURS (pdf)
    * Synthèse des propriétés sur les quadrilatères particuliers
    * TD : plan de travail, avec son corrigé
    
    

Le cours sur le repérage est réduit à peu de choses : savoir se repérer dans le plan, connaître le vocabulaire de base, les différents types de repères. Et deux formules, l'une donnant les coordonnées du milieu d'un segment, l'autre la distance entre deux points.

> *En pratique, penser à vérifier régulièrement la cohérence entre les calculs et la figure...*

??? question "Vérifie que tu connais ces formules et que tu sais les utiliser avec $R(-2;-1)$ et $S(1;3)$"
    * Coordonnées du milieu de $RS$ (qu'on note ici $M$) :     
          $x_M = \dfrac{x_R + x_S}{2} = \dfrac{-2 + 1}{2} = -\dfrac{1}{2}\quad$ et 
          $\quad y_M = \dfrac{y_R + y_S}{2} = \dfrac{-1 + 3}{2} = {1}$    
          D'où : $\quad M(-\frac{1}{2};1)$
          
    * Distance entre deux points (ou longueur d'un segment) :        
          _Attention, cette formule n'est valable que dans un **repère orthonormé**._    
          $RS = \sqrt{(x_S - x_R)^2 + (y_S - y_S)^2} = \sqrt{(1 - (-2))^2 + (3 - (-1))^2} = \sqrt{3^2+4^2} = \sqrt{25} = 5$

Pour pouvoir utiliser efficacement ces propriétés, il faut bien connaître les propriétés de base de la géométrie de collège (dont ce qui concerne les quadrilatères, triangles, les théorèmes de Pythagore ou Thalès...) On complète au passage ces connaissances avec la notion de **projeté orthogonal** d'un point sur une droite.
