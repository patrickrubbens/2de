---
title : Ensembles de nombres - Intervalles
author: Jean-Marc Gervais
---

??? note "Documents en lien avec cette partie (en PDF)"
    
    * Le **cours** est récupérable [ici](../00_Docs/#02_ensembles_intervalles){:target="_blank"}
    
    * Vous trouverez également des exemples de ce qui est attendu dans les deux premiers 
    [devoirs](../00_Docs/#00_Devoirs_et_Corriges){:target="_blank"} dans le 
    dossier dédié.

Dans ce chapitre, il aura peu de difficultés de compréhension, mais attention à ne pas
se laisser surprendre, car il y a plusieurs notions et notations à **mémoriser** !
On rappelle qu'il faut y revenir régulièrement (fréquemment au début, puis en
espaçant progressivement), en se forçant à écrire ce qu'il nous reste en 
mémoire, *avant* d'aller vérifier et si besoin corriger, compléter, à l'aide
du cours. Ces conseils ne sont pas *ma* vision des choses, ils se basent sur 
des [résultats d'études scientifiques rigoureuses](https://scienceetonnante.com/2023/09/01/mieux-apprendre-etudier-les-vraies-techniques-scientifiques/){:target="_blank"}...


On y parle également de la **nature des nombres** (décimaux, rationnels, 
entiers, etc.) Cette notion est impotante notamment quand on s'intéresse à
la manière dont on peut (ou non) les représenter sur un ordinateur...

--- 
À ce propos, quelles sont les notations utilisées pour les ensembles qui
regroupent des nombres de même nature, ainsi que l'ensemble vide ?
Comment note-t-on qu'un nombre *appartient* à un ensemble ? Ou qu'un ensemble
est un sous-ensemble d'un autre ?

??? question "*Écris*-les *avant* de vérifier..."
    * $\mathbb{N}$ est l'ensemble des **entiers naturels** (positifs). Par exemple,
      on peut dire que zéro appartient à cet ensemble, mais pas $-3$, en 
      notant :    $\quad0\in\mathbb{N}$, $\quad-3\notin\mathbb{N}$.
    * $\mathbb{Z}$ est celui des **entiers relatifs** (postifs ou négatifs).     
      $\mathbb{N}$ **est un sous-ensemble de** $\mathbb{Z}$, on peut le noter 
      $\mathbb{N} \subset \mathbb{Z}$, on dit aussi que $\mathbb{N}$ **est
      inclus dans** $\mathbb{Z}$.
    * $\mathbb{D}$ est l'ensemble des **décimaux**, ceux qui ont une écriture à
      virgule finie (pas infinie), qu'on peut écrire $\dfrac{a}{10^n}$ 
      avec $a$ entier et $n\geqslant0$.
    * $\mathbb{Q}$ est l'ensemble des **rationnels**, ceux qu'on peut écrire sous
      forme d'une fraction (quotient de deux entiers).
    * $\mathbb{R}$ est l'ensemble des **réels** (*tous les nombres que tu 
      connais !*).     
      Ceux qui sont dans $\mathbb{R}$ mais pas dans $\mathbb{Q}$
      sont appelés les **irrationnels**(ex. $\sqrt{2}$, $\pi$...)
    * On peut restreindre ces ensembles en ajoutant derrière $+$ (pour ne 
      garder que les positifs, ainsi $\mathbb{Z}^\text{+}=\mathbb{N}$), ou 
      $-$ pour ne garder que les négatifs, et/ou ＊ pour exclure zéro
      (ex. $\mathbb{R}^\text{+＊} = ]0;+\infty[~$).
    * L'ensemble vide se note $\varnothing$.

La partie sur les **intervalles**, leurs **réunions ou intersections**,
est utile dans divers chapitres, comme les **fonctions** (pour leur domaine
de définition par exemple), les résolutions d'**(in)équations** (pour 
l'ensemble des solutions), etc.

Pour vérifier que c'est acquis : comment noter...

* L'ensemble $A$ des nombres réels situés entre $2$ et $5$ compris ?
* $B$, celui des réels strictement supérieurs à $66$ ?
* $C$, celui des réels négatifs ?
* $D$, celui qui contient les nombres $5$ et $9$ (et aucun autre) ?
* $E$, celui des réels situés entre $-1$ compris et $3$ non compris ?

Et n'y a-t-il pas un intrus dans cette liste ?

??? question "Idem, note-les *avant* de vérifier..."
    * $A ~=~ \left[2;5\right]$
    * $B ~=~ \left]66;+\infty\right[$
    * $C ~=~ \left]-\infty;0\right]\quad$  ($0$ est à la fois positif *et* négatif...)
    * $D ~=~ \left\{5;9\right\}\quad$
    * $E ~=~ \left[-1;3\right[$    
    ---
    *$D$ n'est pas un intervalle, c'est lui l'intrus !*
    
Et comment note-t-on l'intersection, la réunion de deux ensembles ?    
Que vaut la réunion, ou l'intersection de $\left[2;5\right]$ et $\left]3;10\right[$ ?    
Et celles de $\left[0;1\right]$ et $\left[1;6\right[$ ?

??? question "Idem, joue le jeu pour ne pas perdre ton temps..."
    * L'intersection se note avec le symbole $\cap$, la réunion avec $\cup$.
    * $\left[2;5\right]\cap\left]3;10\right[ ~=~ \left]3;5\right]\quad$ et $\quad\left[2;5\right]\cup\left]3;10\right[ ~=~ \left[2;10\right[$.
    * $\left[0;1\right]\cap\left[1;6\right[ ~=~ {\left\{1\right\}}\quad$ (il n'y a qu'un nombre en commun aux deux
      intervalles) et $\quad\left[0;1\right]\cup\left[1;6\right[ ~=~ \left[0;6\right[$.
   


