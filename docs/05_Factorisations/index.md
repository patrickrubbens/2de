---
title : Factorisations — CL2
author: Jean-Marc Gervais
---

??? note "[Documents en lien avec cette partie](../00_Docs/#05_factorisations){:target="_blank"} (en PDF)"
    Ils sont récupérables [ici](../00_Docs/#05_factorisations){:target="_blank"} :
    
    * TD de découverte (n°1) et son corrigé.
    * TD séries d'entraînement, quand on doit trouver uniquement un facteur 
      commune (n°2) et son corrigé
    * TD séries d'entraînement, en utilisant une identité remarquable (n°3)
      et son corrigé
    

Factoriser, c'est obtenir une forme multiplicative. Cela permet notamment parfois de se ramener à une équation « produit nul », ou de simplifier des quotients.

On applique la **stratégie** suivante en deux étapes, après avoir bien repéré les différents termes :

??? question "Écris-la *avant* de vérifier, donne des exemples..."
    * On tente de trouver un **facteur commun**, pour factoriser ensuite
      <center>Rappel : $\quad k a + k b = k(a+b)\quad$ et $\quad k a + k b = k(a+b)$.</center>
      
          - $15ab^2 + 25a^3b = 5ab\times3b + 5ab\times 5a^2 = 5ab(3b+5a^2)$
          
          - $(x+2)(2x-5)-(x+2)(7-4x)$    
            $= (x+2)\left[(2x-5)-(7-4x)\right]$    
            $= (x+2)(2x-5-7+4x)$    
            $= (x+2)(6x-12)$
    * (Si l'on n'avait pas de facteur commun, ou pour aller plus loin,) on regarde si l'on ne peut pas appliquer une **identité remarquable**, le plus souvent $a^2-b^2 = (a+b)(a-b)$.
    
          - $36x^2 - 25 = (6x)^2 - 5^2 = (6x+5)(6x-5)$
          
          - $9x^2-(x+7)^2$    
            $= (3x)^2-(x+7)^2$    
            $= [3x + (x+7)][3x - (x+7)]$    
            $= (3x + x + 7)(3x - x - 7)$    
            $= (4x + 7)(2x-7)$
             
          - Voire    
            $4x^2-12x+9$    
            $= (2x)^2 - 2\times2x\times3 + 3^2$    
            $= (2x-3)^2$
      *Parfois, on retrouve à nouveau un facteur commun aux termes de l'un des 
      facteurs, on peut alors factoriser encore davantage...*
      
!!! success "Pour s'entraîner en ligne"
    
    * Sur [MathsAlea](https://coopmaths.fr/alea/?uuid=2e5df&id=2N40-5&n=10&d=10&s=4&alea=AEci&cd=1&uuid=51360&id=3L11-6&n=10&d=10&s=3&alea=dhBh&cd=1&uuid=ecf62&id=3L14&n=5&d=10&s=8&alea=5Vry&cd=1&uuid=04b0a&id=2N41-6&n=10&d=10&s=4&s2=4&alea=jaAe&cd=1&uuid=81fd2&id=3L12&n=10&d=10&s=4&s2=true&alea=Rkg6&cd=1&uuid=874e8&id=2N41-7&n=10&d=10&s=4&alea=OL9T&cd=1&uuid=0bd00&id=2N41-7v2&n=10&d=10&s=4&alea=4kzc&cd=1&v=eleve&title=Identit%C3%A9s+-+Factorisations&es=021100){:target="_blank"}
    * Sur MathEnPoche, [avec un facteur commun](https://mathenpoche.sesamath.net/?page=seconde#seconde_2_8_1_sesabibli/5c9e3226d9e4237dfac4852b){:target="_blank"}, ou [avec une identité remarquable](https://mathenpoche.sesamath.net/?page=seconde#seconde_2_8_1_sesabibli/5c978426d9e4237dfac484f1){:target="_blank"} (ou d'autres, à choisir [ici](https://mathenpoche.sesamath.net/?page=seconde#seconde_2_8_1){:target="_blank"})
