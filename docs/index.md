---
title : Accueil
author: Jean-Marc Gervais
---

# Bienvenue !

Ce petit site vous permet

* d'accéder à une **présentation rapide des différents chapitres** abordés 
(au cours de l'année 2023-2024, en **seconde** générale au
**lycée Jean Monnet** d'Annemasse, France, Haute-Savoie 74)
via les thèmes proposés dans le menu,
* de retrouver directement l'ensemble des documents utilisés dans l'année, 
**devoirs, TP, et leurs corrigés, cours**, etc. dans la dernière rubrique 
« [⛭ Documents distribués](../00_Docs/) »,

* ou encore de retrouver des **outils en ligne** via la page 
« [ⴲ Boîte à Outils](../99_Outils/) » dédiée.

<center>
[![logo Framasoft](Coin_coin_Framasoft.png){min-width=100px}](https://soutenir.framasoft.org/fr/){:target="_blank"}
</center>

<!--
!!! warning "Site en cours d'écriture..."
    *Le contenu est complété au fur et à mesure, il faudra parfois attendre un 
    peu pour que tout soit à jour, merci pour votre compréhension.*
-->

??? info "Crédits & vie privée (*cliquez pour déplier*)"
    Ces pages sont réalisées avec [MkDocs-Material](https://squidfunk.github.io/mkdocs-material/){:target="_blank"}
    (conçu pour réaliser des sites statiques de documentation) + 
    [Pyodide](https://bouillotvincent.gitlab.io/pyodide-mkdocs/){:target="_blank"} 
    (pour l'intégration de code exécutable Python côté client).

    Elles existent donc grâce à l'énorme travail fourni par les auteurs de ces 
    logiciels libres, ainsi que les nombreuses ressources et connaissances 
    partagées par plusieurs collègues, notamment au sein de 
    l'[AEIF](https://aeif.fr/){:target="_blank"} (Association des enseignantes 
    et enseignants d'informatique de France). L'AEIF fournit également la forge
     `gitlab` qui héberge ce site. Merci à toutes et tous !

    Sauf mention contraire, les contenus sont réutilisables sous les conditions
    de la licence [Creative Commons BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr){:target="_blank"}.
    Si j'ai inclus du contenu dont vous étiez l'autrice ou l'auteur sans vous
    mentionner, veuillez m'en excuser et je rectifierai le tir au plus vite si 
    vous m'en informez.

    Ces pages sont conformes au RGPD et respectent votre vie privée. Rien 
    n'est fait pour vous identifier, aucune donnée personnelle n'est collectée.
    
??? info "Sources"
    Les collègues qui désirent les sources (`.odt` ou `.tex`, etc.) des 
    documents distribués aux élèves et mis à disposition ici sous forme de 
    PDF peuvent me contacter en utilisant leur adresse accadémique, sur la 
    mienne (Jean-Marc Gervais, dans l'académie de Grenoble).

    Si vous prenez le temps de me signaler les coquilles et autres points de 
    désaccord, qui permettent d'améliorer le contenu, je vous en serai sincèrement
    très reconnaissant ! Idem si vous trouvez des ressources intéressantes qui 
    mériteraient d'être référencées ici.

