---
title : Algorithmes et Python
author: Jean-Marc Gervais
---

## Outils et ressources
Pour savoir avec quels outils programmer en Python, ou trouver des sites pour
découvrir ce langage de manière progressive voire ludique, tu peux consulter la 
rubrique « [ⴲ Boîte à outils](../99_Outils/#programmation-python) »

Pour retrouver les *notebooks* utilisés en classe pour découvrir le langage, 
c'est dans « [⛭ Documents distribués](../00_Docs/#42_python) »

## Mémo

Un petit [aide-mémoire](../00_Docs/42_Python/Memo_Python.pdf){:target="_blank"} 
avec les quelques structures de base peut t'être bien 
utile...
