---
title : Calculs numériques
author: Jean-Marc Gervais
---

Cette thématique a été en partie étudiée en début d'année, puis complétée un
peu plus tard, même si tout est regroupé ici, afin de repérer plus facilement
l'ensemble des techniques à acquérir.

??? note "[Documents en lien avec cette partie](../00_Docs/#01_calcul_numerique){:target="_blank"} (en PDF)"
    Ils sont récupérables [ici](../00_Docs/#01_calcul_numerique){:target="_blank"} :
    
    * Les **cours** : un premier (numérotation **01.1._n_**) sur les priorités, 
    les nombres relatifs et les fractions ; un second  (en **01.2._n_**) sur
    les puissances et les racines carrées, après  quelques rappels sur les 
    valeurs approchées : arrondis, etc.)
    
    * Le TD qui reprend les « exercices flash » de début d'heure (en début 
    d'année, sur les entiers relatifs, puis les fractions), en rappelant
    comment s'entraîner en autonomie, en vérifiant à la calculatrice, ainsi que
    l'**objectif minimal à atteindre** sans faute et la manière de vérifier 
    qu'on l'a bien atteint.
    
    * Des TD avec des **séries de calculs corrigées** (issues du site MathAléa,
    souvent adaptés),  si l'on ne peut pas ou si l'on ne souhaite pas travailler 
    en ligne (*numérotation analogue à celle des cours associés*).


???+ info "Rappel"
    Une grande dextérité dans les calculs n'est plus un objectif, à l'heure 
    des ordinateurs, calculatrices et smartphones omniprésents. Mais pour 
    pouvoir les utiliser correctement, donner du sens aux calculs effectués,
    prendre le recul nécessaire pour aborder de nouvelles notions, **un 
    minimum de connaissances et d'automatismes sont indispensables**. 
    
    On pourra se contenter de calculs simples, mais **seul un entrainement
    régulier** en parallèle avec un travail de **mémorisation précise** des 
    techniques utlisées permettront d'atteindre ce niveau de base, sans lequel
    les progrès seront fortement compromis dans tous les autres domaines.
    
    Nous avons repris des techniques de cinquième et quatrième. Il est donc 
    plus qu'urgent de les maîtriser...

## Priorités opératoires, nombres relatifs

Pour l'essentiel :

* L'ordre dans lequel on effectue les opérations peut modifier le résultat. 
On doit traiter, dans cet ordre (pour un niveau donné, on calcule dans l'ordre
d'écriture) :
??? question "[Pour mieux mémoriser](https://scienceetonnante.com/2023/09/01/mieux-apprendre-etudier-les-vraies-techniques-scientifiques/){:target="_blank"}, prends le temps de les noter, *avant* de vérifier..."

    1. Les calculs entre parenthèses (ou crochets, en commençant par les plus « intérieurs »)
    2. Les puissances
    3. Les multiplications et divisions
    4. Les additions et soustractions
    
    *Remarque : les fractions « sous-entendent » des parenthèses, car le 
    numérateur, le dénominateur et la fraction forment des blocs qu'on ne peut 
    pas « casser »* : 
    $1 \div \frac{2 + 3}{4 + 5} = 1 \div \left[ (2 + 3) \div (4 + 5) \right]$
  
* Pour le calculs avec des relatifs (des nombres positifs ou négatifs), il faut
connaître ses tables de multiplications (!) et connaître sans hésitation les 
règles suivantes :

??? question "Idem, prends le temps de passer par l'écrit *avant* de vérifier !"

    * La « **règle des signes** » est en général bien connue, mais elle ne 
    s'applique que dans certaines situations : **multiplications**, **divisions**, 
    ou simplifications de signes $+$ ou $-$ successifs ($1 - (-2) = 1 + 2$...)     
    On peut la résumer, de manière symbolique par :
        - ⴲ × ⴲ = ⴲ
        - ⴱ × ⴱ = ⴲ  (*le produit de 2 nombres de même signe est positif*)
        - ⴲ × ⴱ = ⴱ
        - ⴱ × ⴲ = ⴱ (*le produit de 2 nombres de signes contraires est négatif*)
         
    * Pour les **additions**, on peut expliquer les techniques en interprétant
    les calculs en termes d'augmentations / diminutions, ou de montées / 
    descentes, ou autre...
        - Pour additionner 2 nombres **de même signe**, il faut **conserver ce 
        signe** pour la somme, dont la valeur absolue s'obtient 
        **en additionnant les valeurs absolues** des 2 termes additionnés.
        - Pour additionner deux nombres **de signe contraire**, la somme a 
        **le signe de celui qui a la plus grande valeur absolue** et on trouve
        sa valeur absolue **en soustrayant les valeurs absolues** des 2 termes.
        
    * Pour les **soustractions**, on peut se ramener aux additions en se 
    rappelant que **« soustraire un nombre, c'est additionner son opposé »**.
    
    
!!! success "Pour s'entraîner"

    * [Entraînement en ligne sur MathAléa](https://coopmaths.fr/alea/?uuid=c8f4b&id=4C10-5&alea=KZQR&uuid=745ba&id=4C10&n=10&d=10&s=3&s2=1&alea=96qo&cd=1&uuid=0b020&id=4C10-7&n=20&d=10&s=false&s2=5&s3=10&alea=Wlki&cd=1&uuid=62f66&id=4C11&alea=MZCJ&uuid=73187&id=4C10-6&n=5&d=10&s=5&alea=gFcd&cd=0&v=confeleve&v=eleve&title=Relatifs et Priorités&es=021100){:target="_blank"}
    * Des [pyramides de sommes ou produits](https://mathix.org/pyramidecalcul/){:target="_blank"}
    à imprimer ou recopier, pour s'entraîner sur les calculs avec des relatifs.
    * *Et bien sûr les documents distribués en classe et rappelés ci-dessus*...

## Fractions

???+ info "Rappels"

    * Certaines fractions sont des nombres décimaux, elles ont
    donc une écriture décimale (« à virgule ») finie : ce sont celles qu'ont peut
    écrire, une fois simplifiées, avec un dénominateur qui peut s'écrire comme 
    produit de $2$ et de $5$ (facteurs qu'on peut répéter : $200 = 2^3\times5^2$). 
    Par exemple, $\frac{3}{200}$ s'écrit aussi $\frac{15}{1 000} = 0,015$.    
      Les autres n'ont pas d'écriture décimale finie, on laissera donc leur 
    écriture sous forme fractionnaire : $\frac{1}{3}\approx 0,33$ mais 
    $\frac{1}{3}$ et $0,33$ (ou $0,33333$) sont des nombres différents.

    * Les règles de calculs s'appliquent de la même manière aux *quotients* de nombres
    non entiers (mais on ne parle de *fraction* que pour les quotients d'entiers).
    
Pour les techniques de calculs, il n'y a rien de difficile puisque c'est 
« mécanique », toujours pareil, mais il faut les connaître sans hésitation 
pour ne pas tout mélanger :

??? question "À nouveau, prends le temps de passer par l'écrit *avant* de vérifier !"

    * On peut **transformer une fraction en une autre égale** en multipliant (ou en 
    divisant) son numérateur et son dénominateur par un même nombre non nul :
    <center>$\dfrac {a}{b} = \dfrac {k a}{k b}$</center>
    On s'en sert pour simplifier les fractions, ou pour les réduire au même 
    dénominateur.
    
    * S'il y a **des entiers ou des décimaux** qui interviennent, on les 
    écrit d'abord comme des fractions : $\quad 3 = \frac{3}{1}~\text{;}\quad
    2,9 = \frac{29}{10}$
    
    * Pour **additionner ou soustraire** des fractions, on les réduit au même 
    dénominateur (qui sera aussi celui de la somme ou de la différence), puis 
    on additionne (ou soustrait) les numérateurs.
    <center>$\dfrac {a}{D} + \dfrac {b}{D} = \dfrac {a + b}{D} \quad \text{et}
    \quad \dfrac {a}{D} - \dfrac {b}{D} = \dfrac {a - b}{D}$</center>
    
    * Pour **multiplier** des fractions, c'est le plus simple, on multiplie 
    les numérateurs entre eux et les dénominateurs entre eux. 
    <center>$\dfrac {a}{b} \times \dfrac {c}{d} = \dfrac {a c}{b d}$</center>
    Il est conseillé,
    en se rappelant que ça n'est possible qu'avec les multiplications, de 
    simplifier *avant* d'effectuer les calculs.    
    $\qquad\frac {75}{8} \times \frac{4}{125}$    
    $\qquad= \frac {\mathbf{25}\times3}{\mathbf{4}\times2} \times
     \frac{\mathbf{4}\times1}{\mathbf{25}\times5}$    
    $\qquad= \frac {3}{2} \times \frac{1}{5}$    
    $\qquad= \frac {3}{10}$
    
    * Pour **diviser**, on se ramène à une multiplication : diviser par un
    nombre, c'est multiplier par son inverse (et ici, l'inverse de $\frac{C}{D}$
    est $\frac{D}{C}$) :
    <center>$\dfrac {\frac{A}{B}} {\frac{C}{D}}
    = \dfrac{A}{B} \div \dfrac{C}{D} 
    = \dfrac{A}{B} \times \dfrac{D}{C}\cdots$</center>
    

!!! success "Pour s'entraîner en ligne"

    [Entraînement en ligne sur MathAléa](https://coopmaths.fr/alea/?uuid=5f429&id=4C21&n=5&d=10&s=2&s2=false&s3=true&s4=true&alea=akZZ&cd=1&uuid=5f429&id=4C21&n=5&d=10&s=2&s2=true&s3=true&s4=true&alea=rWmW&cd=1&uuid=72ce7&id=4C22&n=5&d=10&s=2&s2=true&s3=true&s4=1&alea=IkyZ&cd=1&uuid=55354&id=4C22-2&alea=mI0w&uuid=55354&id=4C22-2&n=5&d=10&s=2&alea=GHVn&cd=1&uuid=72ce7&id=4C22&n=10&d=10&s=2&s2=true&s3=true&s4=3&alea=HZJF&cd=1&uuid=374b6&id=4C23&n=10&d=10&s=1-2-3&alea=TCKz&cd=1&cols=5&uuid=18ddd&id=4C23-1&n=5&d=10&s=3&s2=false&s3=true&s4=true&alea=V86T&cd=1&uuid=18ddd&id=4C23-1&n=5&d=10&s=2&s2=true&s3=false&s4=true&alea=NhpO&cd=1&v=eleve&title=Fractions&es=021100){:target="_blank"}

## Puissances

Comme les multiplications permettent d'écrire de manière simplifiée des
répétitions d'additions identiques, les puissances permettent d'écrire de
manière simplifiée des répétitions de multiplications identiques.

Par exemple, par définition, $2^5$, $(-5)^3$, $4^{-2}$, $17^1$ ou $(-6)^0$ signifient...

??? question "Écris-le *avant* de vérifier ici !"
    * $2^5 = 2\times2\times2\times2\times2 = 32$
    * $(-5)^3 = -5 \times (-5) \times (-5) = -125$
    * $4^{-2} = \dfrac{1}{4^2} = \dfrac{1}{4\times4} = \dfrac{1}{16}$ ; de manière
      générale, $x^{-n} = \dfrac{1}{x^n}$ pour tout $x \neq 0$
    * $17^1 = 17$ et de manière générale, $x^{1} = x$
    * $(-6)^0 = 1$ et de manière générale, $x^{0} = 1$, sauf pour $0^0$ qui 
      n'est pas défini
    
De part leur définition basée sur des multiplications, les puissances ont
des propriétés intéressantes qui permettent de simplifier les calculs dans les
multiplications et les divisions et au contraire n'ont pas de propriété 
intéressante avec les additions et soustractions (*on explique ces propriétés
en revenant à la définition, quitte à étudier séparément les différents cas,
suivant les signes des exposants*) :

??? question "Note-les (formule, ou exemple) pour travailler ou vérifier leur mémorisation"
    Pour tous réels $x$, $y$ et tous entiers $n$ et $m$ :
    
    * $x^n \times x^m = x^{n+m}$, d'où par exemple : 
      $\quad(-2)^4 \times (-2)^7 = (-2)^{11}$
    * $\dfrac{x^n}{x^m} = x^{n-m}$ pour tout $x \neq 0$, par exemple 
      $\dfrac{10^9}{10^{-3}}=10^{9-(-3)}=10^{12}$
    * $(x \times y)^n = x^n \times y^m$ : $\quad 5^2\times 2^2 = 10^2$
    * $\dfrac{x^n}{y^n} = \left(\dfrac{x}{y}\right)^n$ pour tout $y \neq 0$ :
      $\quad\dfrac{2^3}{7^3} = \left(\dfrac{2}{7}\right)^3$
    * $(x^n)^m = x^{n + m}$ : $\quad \left(2^5\right)^3 = 2^{15}$
    
Bien entendu, on n'oublie pas le cas particulier des puissances de $10$, bien
utiles notamment dans les écritures scientifiques, sans mélanger avec ce qui
précède. 
    
??? question "Tes souvenirs, sur ces sujets ?"
    * Pour $n$ positif : 
    
        - $10^n = 100000...00$ ($1$, suivi de $n$ zéros)
        - $10^{-n} = 0,0000...01$ (avec $n$ chiffres après la virgule)
        
    * Écriture scientifique : $±a \times 10^n$ où $a$ s'écrit avec un unique
      chiffre différent de zéro avant la virgule

!!! success "Pour s'entraîner en ligne"

    [Entraînement en ligne sur MathAléa](https://coopmaths.fr/alea/?uuid=53fbb&id=2N31-0&n=10&d=10&s=true&alea=y8kk&cd=1&cols=5&uuid=c9404&id=2N31-1&n=10&d=10&s=2&s2=3&alea=wj6E&cd=1&cols=5&uuid=c9404&id=2N31-1&n=10&d=10&s=1&s2=3&alea=4UVC&cd=1&cols=5&uuid=c71da&id=4C33-3&n=10&d=10&alea=Qlh9&cd=1&cols=5&uuid=1e42b&id=2N31-2&n=10&d=10&s=5&s2=3&alea=TWv5&cd=0&cols=4&uuid=6b2e5&n=10&d=10&s=3&s2=4&alea=Wuol&cd=0&cols=4&uuid=816c8&n=10&d=10&s=3&s2=3&s3=3&alea=8t2p&cd=1&cols=5&uuid=fedae&id=4C32-2&n=10&d=10&alea=AJCt&cd=0&cols=2&v=eleve&title=Puissances&es=021100){:target="_blank"}

## Racines carrées

Il est important de savoir précisément de quoi on parle, donc de savoir définir
la racine carrée d'un réel $x$ donné...

??? question "Alors (note d'abord cette définition, *puis* vérifie) ?"

    * Comme le carré d'un réel est positif, **la racine carrée d'un nombre 
      strictement négatif n'est pas définie**, $\sqrt{x}$ n'existe pas si $x$ 
      est strictement négatif.
      
    * La racine carrée d'un nombre positif $x$ est **le réel _positif_ dont le 
      carré vaut $x$**. On le note $\sqrt{x}$ (le symbole utilisé est appelé le 
      radical)    
      *Attention, le terme « positif » intervient à deux niveaux ici, prends le
      temps de bien assimiler cette définition*.    
      Par exemple, $(-3)^2=9$ et $3^2=9$, mais seul $3$ est la racine carrée de
      $9$, car $-3$ est négatif. 
    
Quand ce ne sont pas des entiers, les racines  carrées d'entiers sont des 
irrationnels (par exemple, $\sqrt{2} \notin \mathbb{Q}$). Du coup, on laisse 
l'écriture avec le radical pour écrire leur valeur exacte.

Il existe des formules qui permettent de transformer les écritures avec des 
racines carrées, notamment en présence de multiplications ou de divisions 
(mais un peu comme pour les puissances, aucune avec des additions ou 
soustractions).

??? question "Là aussi, note ces formules ou des exemples, puis vérifie..."

    * **La plus importante** : $\quad\sqrt{a \times b} = \sqrt{a} \times \sqrt{b}$.    
      Exemple : $\quad\sqrt{200} = \sqrt{100\times2} = \sqrt{100} \times \sqrt{2} = 10\sqrt{2}$
    * $\sqrt{\dfrac{a}{b}} = \dfrac{\sqrt{a}}{\sqrt{b}}\text{.}\quad$ Par exemple, 
      $\quad \sqrt{\dfrac{25}{4}} = \dfrac{\sqrt{25}}{\sqrt{4}} = \dfrac{5}{4}$
    * **_Attention, rappel_** : $\quad\sqrt{a+b} \neq \sqrt{a} + \sqrt{b}\quad$ et $\quad\sqrt{a-b} \neq \sqrt{a} - \sqrt{b}\quad$
    * D'après la définition de la racine carrée, pour tout $a \geqslant 0$, on a : $\quad{\sqrt{a}}^{\,2}=a$.
    * Pour tout réel $a$, on a : $\quad\sqrt{a^2} = \left|{a}\right|\quad$ (*ne  pas oublier la valeur absolue*).
        
Les applications de base consistent à passer d'une écriture du type $\sqrt{n}$ 
(avec $n$ entier), à $a\sqrt{b}$ (avec $a$ et $b$ entiers, $b$ le plus petit 
possible (on appelle cela « simplifier » la racine carrée), ou à effectuer 
l'opération inverse. La simplification permet parfois de réduire des sommes 
de termes qui comportent des racines carrées :    
$2\sqrt{75} + \sqrt{48} - 3\sqrt{8} = 2\times 5 \sqrt{3} + 4 \sqrt{3} - 
3\times 2\sqrt{2} = 14 \sqrt{3} - 6\sqrt{2}$
      
!!! success "Pour s'entraîner en ligne"

    [Entraînement en ligne sur MathAléa](https://coopmaths.fr/alea/?uuid=53fbb&id=2N31-0&n=10&d=10&s=true&alea=y8kk&cd=1&cols=5&uuid=c9404&id=2N31-1&n=10&d=10&s=2&s2=3&alea=wj6E&cd=1&cols=5&uuid=c9404&id=2N31-1&n=10&d=10&s=1&s2=3&alea=4UVC&cd=1&cols=5&uuid=c71da&id=4C33-3&n=10&d=10&alea=Qlh9&cd=1&cols=5&uuid=1e42b&id=2N31-2&n=10&d=10&s=5&s2=3&alea=TWv5&cd=0&cols=4&uuid=6b2e5&n=10&d=10&s=3&s2=4&alea=Wuol&cd=0&cols=4&uuid=816c8&n=10&d=10&s=3&s2=3&s3=3&alea=8t2p&cd=1&cols=5&uuid=fedae&id=4C32-2&n=10&d=10&alea=AJCt&cd=0&cols=2&v=eleve&title=Puissances&es=021100){:target="_blank"}


