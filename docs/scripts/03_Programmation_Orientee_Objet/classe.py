class Joueur:
    """Classe représentant un joueur.
    
    On doit indiquer le pseudo lors de l'instanciation.
    Le score du joueur est initialement de zéro.
    """
    
    def __init__(self, pseudo):
        self.score = 0
        self.pseudo = pseudo
        
    def gagner_points(self, valeur):
        """Ajoute le nombre de points en paramètre au score du joueur.
        """
        self.score += valeur
