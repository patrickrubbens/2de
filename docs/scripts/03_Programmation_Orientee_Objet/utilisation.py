# Il faut avoir défini la classe Joueur et ses instances alice et bob avant !

for _ in range(4):
    alice.gagner_points(42)
    
bob.score = 66   # Accès direct à l'attribut possible en Python !

for joueur in (alice, bob):
    pluriel = "s" if joueur.score > 1 else ""
    print(f'Score de "{joueur.pseudo}" : {joueur.score} point{pluriel}')
