Site fournissant quelques ressources à mes élèves de seconde au lycée Jean Monnet d'Annemasse (France, Haute-Savoie 74).

Son rendu est visible [sur https://gervaisprof.forge.aeif.fr/2de](https://gervaisprof.forge.aeif.fr/2de/).

**Merci** au passage aux contributeurs et contributrices pour les nombreux outils et services développés et mis à disposition, pour toutes ces connaissances partagées ! Un grand merci également à l'AEIF qui fédère toutes ces bonnes vonlontés et offre sa forge pour héberger ce site.
